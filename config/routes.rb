Rails.application.routes.draw do
  
  resources :mahasiswas
  resources :mata_kuliahs
  resources :proyek_pendidikans
  resources :program_studis
  resources :kelas
  resources :dosens
  get 'welcome/index'
  root to: 'homes#index'
  devise_for :users,
  
  controllers: { sessions: 'users/sessions' }   
  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  resources :homes
  

end
