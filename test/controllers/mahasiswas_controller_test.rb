require 'test_helper'

class MahasiswasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mahasiswa = mahasiswas(:one)
  end

  test "should get index" do
    get mahasiswas_url
    assert_response :success
  end

  test "should get new" do
    get new_mahasiswa_url
    assert_response :success
  end

  test "should create mahasiswa" do
    assert_difference('Mahasiswa.count') do
      post mahasiswas_url, params: { mahasiswa: { agama: @mahasiswa.agama, alamat: @mahasiswa.alamat, alamat_ortu: @mahasiswa.alamat_ortu, alamat_pekerjaan: @mahasiswa.alamat_pekerjaan, alamat_pekerjaan_ibu: @mahasiswa.alamat_pekerjaan_ibu, desa_kelurahan: @mahasiswa.desa_kelurahan, desa_kelurahan_ortu: @mahasiswa.desa_kelurahan_ortu, email: @mahasiswa.email, email_ayah: @mahasiswa.email_ayah, email_ibu: @mahasiswa.email_ibu, gol_darah: @mahasiswa.gol_darah, jalur_masuk: @mahasiswa.jalur_masuk, jenis_kelamin: @mahasiswa.jenis_kelamin, kecamatan: @mahasiswa.kecamatan, kecamatan_ortu: @mahasiswa.kecamatan_ortu, kode_pos: @mahasiswa.kode_pos, kode_pos_ortu: @mahasiswa.kode_pos_ortu, kota_kab: @mahasiswa.kota_kab, kota_kab_ortu: @mahasiswa.kota_kab_ortu, nama: @mahasiswa.nama, nama_lengkap_ayah: @mahasiswa.nama_lengkap_ayah, nama_lengkap_ibu: @mahasiswa.nama_lengkap_ibu, nem: @mahasiswa.nem, nilai_toefl: @mahasiswa.nilai_toefl, no_hp: @mahasiswa.no_hp, no_hp_ibu: @mahasiswa.no_hp_ibu, no_ijazah: @mahasiswa.no_ijazah, nrp: @mahasiswa.nrp, pekerjaan: @mahasiswa.pekerjaan, pekerjaan_ibu: @mahasiswa.pekerjaan_ibu, pendidikan_terakhir: @mahasiswa.pendidikan_terakhir, pendidikan_terakhir_ibu: @mahasiswa.pendidikan_terakhir_ibu, provinsi: @mahasiswa.provinsi, provinsi_ortu: @mahasiswa.provinsi_ortu, rhesus: @mahasiswa.rhesus, status_mahasiswa: @mahasiswa.status_mahasiswa, tanggal_ijazah: @mahasiswa.tanggal_ijazah, tanggal_lahir: @mahasiswa.tanggal_lahir, tanggal_lahir_ayah: @mahasiswa.tanggal_lahir_ayah, tanggal_lahir_ibu: @mahasiswa.tanggal_lahir_ibu, tempat_lahir: @mahasiswa.tempat_lahir, tempat_lahir_ayah: @mahasiswa.tempat_lahir_ayah, tempat_lahir_ibu: @mahasiswa.tempat_lahir_ibu, warga_negara: @mahasiswa.warga_negara } }
    end

    assert_redirected_to mahasiswa_url(Mahasiswa.last)
  end

  test "should show mahasiswa" do
    get mahasiswa_url(@mahasiswa)
    assert_response :success
  end

  test "should get edit" do
    get edit_mahasiswa_url(@mahasiswa)
    assert_response :success
  end

  test "should update mahasiswa" do
    patch mahasiswa_url(@mahasiswa), params: { mahasiswa: { agama: @mahasiswa.agama, alamat: @mahasiswa.alamat, alamat_ortu: @mahasiswa.alamat_ortu, alamat_pekerjaan: @mahasiswa.alamat_pekerjaan, alamat_pekerjaan_ibu: @mahasiswa.alamat_pekerjaan_ibu, desa_kelurahan: @mahasiswa.desa_kelurahan, desa_kelurahan_ortu: @mahasiswa.desa_kelurahan_ortu, email: @mahasiswa.email, email_ayah: @mahasiswa.email_ayah, email_ibu: @mahasiswa.email_ibu, gol_darah: @mahasiswa.gol_darah, jalur_masuk: @mahasiswa.jalur_masuk, jenis_kelamin: @mahasiswa.jenis_kelamin, kecamatan: @mahasiswa.kecamatan, kecamatan_ortu: @mahasiswa.kecamatan_ortu, kode_pos: @mahasiswa.kode_pos, kode_pos_ortu: @mahasiswa.kode_pos_ortu, kota_kab: @mahasiswa.kota_kab, kota_kab_ortu: @mahasiswa.kota_kab_ortu, nama: @mahasiswa.nama, nama_lengkap_ayah: @mahasiswa.nama_lengkap_ayah, nama_lengkap_ibu: @mahasiswa.nama_lengkap_ibu, nem: @mahasiswa.nem, nilai_toefl: @mahasiswa.nilai_toefl, no_hp: @mahasiswa.no_hp, no_hp_ibu: @mahasiswa.no_hp_ibu, no_ijazah: @mahasiswa.no_ijazah, nrp: @mahasiswa.nrp, pekerjaan: @mahasiswa.pekerjaan, pekerjaan_ibu: @mahasiswa.pekerjaan_ibu, pendidikan_terakhir: @mahasiswa.pendidikan_terakhir, pendidikan_terakhir_ibu: @mahasiswa.pendidikan_terakhir_ibu, provinsi: @mahasiswa.provinsi, provinsi_ortu: @mahasiswa.provinsi_ortu, rhesus: @mahasiswa.rhesus, status_mahasiswa: @mahasiswa.status_mahasiswa, tanggal_ijazah: @mahasiswa.tanggal_ijazah, tanggal_lahir: @mahasiswa.tanggal_lahir, tanggal_lahir_ayah: @mahasiswa.tanggal_lahir_ayah, tanggal_lahir_ibu: @mahasiswa.tanggal_lahir_ibu, tempat_lahir: @mahasiswa.tempat_lahir, tempat_lahir_ayah: @mahasiswa.tempat_lahir_ayah, tempat_lahir_ibu: @mahasiswa.tempat_lahir_ibu, warga_negara: @mahasiswa.warga_negara } }
    assert_redirected_to mahasiswa_url(@mahasiswa)
  end

  test "should destroy mahasiswa" do
    assert_difference('Mahasiswa.count', -1) do
      delete mahasiswa_url(@mahasiswa)
    end

    assert_redirected_to mahasiswas_url
  end
end
