require 'test_helper'

class ProyekPendidikansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @proyek_pendidikan = proyek_pendidikans(:one)
  end

  test "should get index" do
    get proyek_pendidikans_url
    assert_response :success
  end

  test "should get new" do
    get new_proyek_pendidikan_url
    assert_response :success
  end

  test "should create proyek_pendidikan" do
    assert_difference('ProyekPendidikan.count') do
      post proyek_pendidikans_url, params: { proyek_pendidikan: { kode_pp: @proyek_pendidikan.kode_pp, proyek_pendidikan: @proyek_pendidikan.proyek_pendidikan } }
    end

    assert_redirected_to proyek_pendidikan_url(ProyekPendidikan.last)
  end

  test "should show proyek_pendidikan" do
    get proyek_pendidikan_url(@proyek_pendidikan)
    assert_response :success
  end

  test "should get edit" do
    get edit_proyek_pendidikan_url(@proyek_pendidikan)
    assert_response :success
  end

  test "should update proyek_pendidikan" do
    patch proyek_pendidikan_url(@proyek_pendidikan), params: { proyek_pendidikan: { kode_pp: @proyek_pendidikan.kode_pp, proyek_pendidikan: @proyek_pendidikan.proyek_pendidikan } }
    assert_redirected_to proyek_pendidikan_url(@proyek_pendidikan)
  end

  test "should destroy proyek_pendidikan" do
    assert_difference('ProyekPendidikan.count', -1) do
      delete proyek_pendidikan_url(@proyek_pendidikan)
    end

    assert_redirected_to proyek_pendidikans_url
  end
end
