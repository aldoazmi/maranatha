require 'test_helper'

class ProgramStudisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @program_studi = program_studis(:one)
  end

  test "should get index" do
    get program_studis_url
    assert_response :success
  end

  test "should get new" do
    get new_program_studi_url
    assert_response :success
  end

  test "should create program_studi" do
    assert_difference('ProgramStudi.count') do
      post program_studis_url, params: { program_studi: { kode_ps: @program_studi.kode_ps, nama_program_studi: @program_studi.nama_program_studi } }
    end

    assert_redirected_to program_studi_url(ProgramStudi.last)
  end

  test "should show program_studi" do
    get program_studi_url(@program_studi)
    assert_response :success
  end

  test "should get edit" do
    get edit_program_studi_url(@program_studi)
    assert_response :success
  end

  test "should update program_studi" do
    patch program_studi_url(@program_studi), params: { program_studi: { kode_ps: @program_studi.kode_ps, nama_program_studi: @program_studi.nama_program_studi } }
    assert_redirected_to program_studi_url(@program_studi)
  end

  test "should destroy program_studi" do
    assert_difference('ProgramStudi.count', -1) do
      delete program_studi_url(@program_studi)
    end

    assert_redirected_to program_studis_url
  end
end
