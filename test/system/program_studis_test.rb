require "application_system_test_case"

class ProgramStudisTest < ApplicationSystemTestCase
  setup do
    @program_studi = program_studis(:one)
  end

  test "visiting the index" do
    visit program_studis_url
    assert_selector "h1", text: "Program Studis"
  end

  test "creating a Program studi" do
    visit program_studis_url
    click_on "New Program Studi"

    fill_in "Kode ps", with: @program_studi.kode_ps
    fill_in "Nama program studi", with: @program_studi.nama_program_studi
    click_on "Create Program studi"

    assert_text "Program studi was successfully created"
    click_on "Back"
  end

  test "updating a Program studi" do
    visit program_studis_url
    click_on "Edit", match: :first

    fill_in "Kode ps", with: @program_studi.kode_ps
    fill_in "Nama program studi", with: @program_studi.nama_program_studi
    click_on "Update Program studi"

    assert_text "Program studi was successfully updated"
    click_on "Back"
  end

  test "destroying a Program studi" do
    visit program_studis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Program studi was successfully destroyed"
  end
end
