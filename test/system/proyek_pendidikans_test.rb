require "application_system_test_case"

class ProyekPendidikansTest < ApplicationSystemTestCase
  setup do
    @proyek_pendidikan = proyek_pendidikans(:one)
  end

  test "visiting the index" do
    visit proyek_pendidikans_url
    assert_selector "h1", text: "Proyek Pendidikans"
  end

  test "creating a Proyek pendidikan" do
    visit proyek_pendidikans_url
    click_on "New Proyek Pendidikan"

    fill_in "Kode pp", with: @proyek_pendidikan.kode_pp
    fill_in "Proyek pendidikan", with: @proyek_pendidikan.proyek_pendidikan
    click_on "Create Proyek pendidikan"

    assert_text "Proyek pendidikan was successfully created"
    click_on "Back"
  end

  test "updating a Proyek pendidikan" do
    visit proyek_pendidikans_url
    click_on "Edit", match: :first

    fill_in "Kode pp", with: @proyek_pendidikan.kode_pp
    fill_in "Proyek pendidikan", with: @proyek_pendidikan.proyek_pendidikan
    click_on "Update Proyek pendidikan"

    assert_text "Proyek pendidikan was successfully updated"
    click_on "Back"
  end

  test "destroying a Proyek pendidikan" do
    visit proyek_pendidikans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Proyek pendidikan was successfully destroyed"
  end
end
