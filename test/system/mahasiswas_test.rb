require "application_system_test_case"

class MahasiswasTest < ApplicationSystemTestCase
  setup do
    @mahasiswa = mahasiswas(:one)
  end

  test "visiting the index" do
    visit mahasiswas_url
    assert_selector "h1", text: "Mahasiswas"
  end

  test "creating a Mahasiswa" do
    visit mahasiswas_url
    click_on "New Mahasiswa"

    fill_in "Agama", with: @mahasiswa.agama
    fill_in "Alamat", with: @mahasiswa.alamat
    fill_in "Alamat ortu", with: @mahasiswa.alamat_ortu
    fill_in "Alamat pekerjaan", with: @mahasiswa.alamat_pekerjaan
    fill_in "Alamat pekerjaan ibu", with: @mahasiswa.alamat_pekerjaan_ibu
    fill_in "Desa kelurahan", with: @mahasiswa.desa_kelurahan
    fill_in "Desa kelurahan ortu", with: @mahasiswa.desa_kelurahan_ortu
    fill_in "Email", with: @mahasiswa.email
    fill_in "Email ayah", with: @mahasiswa.email_ayah
    fill_in "Email ibu", with: @mahasiswa.email_ibu
    fill_in "Gol darah", with: @mahasiswa.gol_darah
    fill_in "Jalur masuk", with: @mahasiswa.jalur_masuk
    fill_in "Jenis kelamin", with: @mahasiswa.jenis_kelamin
    fill_in "Kecamatan", with: @mahasiswa.kecamatan
    fill_in "Kecamatan ortu", with: @mahasiswa.kecamatan_ortu
    fill_in "Kode pos", with: @mahasiswa.kode_pos
    fill_in "Kode pos ortu", with: @mahasiswa.kode_pos_ortu
    fill_in "Kota kab", with: @mahasiswa.kota_kab
    fill_in "Kota kab ortu", with: @mahasiswa.kota_kab_ortu
    fill_in "Nama", with: @mahasiswa.nama
    fill_in "Nama lengkap ayah", with: @mahasiswa.nama_lengkap_ayah
    fill_in "Nama lengkap ibu", with: @mahasiswa.nama_lengkap_ibu
    fill_in "Nem", with: @mahasiswa.nem
    fill_in "Nilai toefl", with: @mahasiswa.nilai_toefl
    fill_in "No hp", with: @mahasiswa.no_hp
    fill_in "No hp ibu", with: @mahasiswa.no_hp_ibu
    fill_in "No ijazah", with: @mahasiswa.no_ijazah
    fill_in "Nrp", with: @mahasiswa.nrp
    fill_in "Pekerjaan", with: @mahasiswa.pekerjaan
    fill_in "Pekerjaan ibu", with: @mahasiswa.pekerjaan_ibu
    fill_in "Pendidikan terakhir", with: @mahasiswa.pendidikan_terakhir
    fill_in "Pendidikan terakhir ibu", with: @mahasiswa.pendidikan_terakhir_ibu
    fill_in "Provinsi", with: @mahasiswa.provinsi
    fill_in "Provinsi ortu", with: @mahasiswa.provinsi_ortu
    fill_in "Rhesus", with: @mahasiswa.rhesus
    fill_in "Status mahasiswa", with: @mahasiswa.status_mahasiswa
    fill_in "Tanggal ijazah", with: @mahasiswa.tanggal_ijazah
    fill_in "Tanggal lahir", with: @mahasiswa.tanggal_lahir
    fill_in "Tanggal lahir ayah", with: @mahasiswa.tanggal_lahir_ayah
    fill_in "Tanggal lahir ibu", with: @mahasiswa.tanggal_lahir_ibu
    fill_in "Tempat lahir", with: @mahasiswa.tempat_lahir
    fill_in "Tempat lahir ayah", with: @mahasiswa.tempat_lahir_ayah
    fill_in "Tempat lahir ibu", with: @mahasiswa.tempat_lahir_ibu
    fill_in "Warga negara", with: @mahasiswa.warga_negara
    click_on "Create Mahasiswa"

    assert_text "Mahasiswa was successfully created"
    click_on "Back"
  end

  test "updating a Mahasiswa" do
    visit mahasiswas_url
    click_on "Edit", match: :first

    fill_in "Agama", with: @mahasiswa.agama
    fill_in "Alamat", with: @mahasiswa.alamat
    fill_in "Alamat ortu", with: @mahasiswa.alamat_ortu
    fill_in "Alamat pekerjaan", with: @mahasiswa.alamat_pekerjaan
    fill_in "Alamat pekerjaan ibu", with: @mahasiswa.alamat_pekerjaan_ibu
    fill_in "Desa kelurahan", with: @mahasiswa.desa_kelurahan
    fill_in "Desa kelurahan ortu", with: @mahasiswa.desa_kelurahan_ortu
    fill_in "Email", with: @mahasiswa.email
    fill_in "Email ayah", with: @mahasiswa.email_ayah
    fill_in "Email ibu", with: @mahasiswa.email_ibu
    fill_in "Gol darah", with: @mahasiswa.gol_darah
    fill_in "Jalur masuk", with: @mahasiswa.jalur_masuk
    fill_in "Jenis kelamin", with: @mahasiswa.jenis_kelamin
    fill_in "Kecamatan", with: @mahasiswa.kecamatan
    fill_in "Kecamatan ortu", with: @mahasiswa.kecamatan_ortu
    fill_in "Kode pos", with: @mahasiswa.kode_pos
    fill_in "Kode pos ortu", with: @mahasiswa.kode_pos_ortu
    fill_in "Kota kab", with: @mahasiswa.kota_kab
    fill_in "Kota kab ortu", with: @mahasiswa.kota_kab_ortu
    fill_in "Nama", with: @mahasiswa.nama
    fill_in "Nama lengkap ayah", with: @mahasiswa.nama_lengkap_ayah
    fill_in "Nama lengkap ibu", with: @mahasiswa.nama_lengkap_ibu
    fill_in "Nem", with: @mahasiswa.nem
    fill_in "Nilai toefl", with: @mahasiswa.nilai_toefl
    fill_in "No hp", with: @mahasiswa.no_hp
    fill_in "No hp ibu", with: @mahasiswa.no_hp_ibu
    fill_in "No ijazah", with: @mahasiswa.no_ijazah
    fill_in "Nrp", with: @mahasiswa.nrp
    fill_in "Pekerjaan", with: @mahasiswa.pekerjaan
    fill_in "Pekerjaan ibu", with: @mahasiswa.pekerjaan_ibu
    fill_in "Pendidikan terakhir", with: @mahasiswa.pendidikan_terakhir
    fill_in "Pendidikan terakhir ibu", with: @mahasiswa.pendidikan_terakhir_ibu
    fill_in "Provinsi", with: @mahasiswa.provinsi
    fill_in "Provinsi ortu", with: @mahasiswa.provinsi_ortu
    fill_in "Rhesus", with: @mahasiswa.rhesus
    fill_in "Status mahasiswa", with: @mahasiswa.status_mahasiswa
    fill_in "Tanggal ijazah", with: @mahasiswa.tanggal_ijazah
    fill_in "Tanggal lahir", with: @mahasiswa.tanggal_lahir
    fill_in "Tanggal lahir ayah", with: @mahasiswa.tanggal_lahir_ayah
    fill_in "Tanggal lahir ibu", with: @mahasiswa.tanggal_lahir_ibu
    fill_in "Tempat lahir", with: @mahasiswa.tempat_lahir
    fill_in "Tempat lahir ayah", with: @mahasiswa.tempat_lahir_ayah
    fill_in "Tempat lahir ibu", with: @mahasiswa.tempat_lahir_ibu
    fill_in "Warga negara", with: @mahasiswa.warga_negara
    click_on "Update Mahasiswa"

    assert_text "Mahasiswa was successfully updated"
    click_on "Back"
  end

  test "destroying a Mahasiswa" do
    visit mahasiswas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Mahasiswa was successfully destroyed"
  end
end
