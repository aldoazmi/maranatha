require "application_system_test_case"

class KelasTest < ApplicationSystemTestCase
  setup do
    @kela = kelas(:one)
  end

  test "visiting the index" do
    visit kelas_url
    assert_selector "h1", text: "Kelas"
  end

  test "creating a Kela" do
    visit kelas_url
    click_on "New Kela"

    fill_in "Kode kelas", with: @kela.kode_kelas
    fill_in "Nama kelas", with: @kela.nama_kelas
    click_on "Create Kela"

    assert_text "Kela was successfully created"
    click_on "Back"
  end

  test "updating a Kela" do
    visit kelas_url
    click_on "Edit", match: :first

    fill_in "Kode kelas", with: @kela.kode_kelas
    fill_in "Nama kelas", with: @kela.nama_kelas
    click_on "Update Kela"

    assert_text "Kela was successfully updated"
    click_on "Back"
  end

  test "destroying a Kela" do
    visit kelas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Kela was successfully destroyed"
  end
end
