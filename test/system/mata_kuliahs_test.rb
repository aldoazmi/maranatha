require "application_system_test_case"

class MataKuliahsTest < ApplicationSystemTestCase
  setup do
    @mata_kuliah = mata_kuliahs(:one)
  end

  test "visiting the index" do
    visit mata_kuliahs_url
    assert_selector "h1", text: "Mata Kuliahs"
  end

  test "creating a Mata kuliah" do
    visit mata_kuliahs_url
    click_on "New Mata Kuliah"

    fill_in "Kode mata kuliah", with: @mata_kuliah.kode_mata_kuliah
    fill_in "Nama mata kuliah", with: @mata_kuliah.nama_mata_kuliah
    fill_in "Prasyarat", with: @mata_kuliah.prasyarat
    fill_in "Semester", with: @mata_kuliah.semester
    fill_in "Sifat", with: @mata_kuliah.sifat
    fill_in "Sks", with: @mata_kuliah.sks
    click_on "Create Mata kuliah"

    assert_text "Mata kuliah was successfully created"
    click_on "Back"
  end

  test "updating a Mata kuliah" do
    visit mata_kuliahs_url
    click_on "Edit", match: :first

    fill_in "Kode mata kuliah", with: @mata_kuliah.kode_mata_kuliah
    fill_in "Nama mata kuliah", with: @mata_kuliah.nama_mata_kuliah
    fill_in "Prasyarat", with: @mata_kuliah.prasyarat
    fill_in "Semester", with: @mata_kuliah.semester
    fill_in "Sifat", with: @mata_kuliah.sifat
    fill_in "Sks", with: @mata_kuliah.sks
    click_on "Update Mata kuliah"

    assert_text "Mata kuliah was successfully updated"
    click_on "Back"
  end

  test "destroying a Mata kuliah" do
    visit mata_kuliahs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Mata kuliah was successfully destroyed"
  end
end
