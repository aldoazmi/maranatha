json.extract! mata_kuliah, :id, :kode_mata_kuliah, :nama_mata_kuliah, :sks, :prasyarat, :semester, :sifat, :created_at, :updated_at
json.url mata_kuliah_url(mata_kuliah, format: :json)
