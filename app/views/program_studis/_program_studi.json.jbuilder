json.extract! program_studi, :id, :kode_ps, :nama_program_studi, :created_at, :updated_at
json.url program_studi_url(program_studi, format: :json)
