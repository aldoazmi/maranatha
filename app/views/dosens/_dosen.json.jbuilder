json.extract! dosen, :id, :nik, :nama_dosen, :email, :created_at, :updated_at
json.url dosen_url(dosen, format: :json)
