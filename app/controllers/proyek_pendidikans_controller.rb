class ProyekPendidikansController < ApplicationController
  before_action :set_proyek_pendidikan, only: [:show, :edit, :update, :destroy]

  # GET /proyek_pendidikans
  # GET /proyek_pendidikans.json
  def index
    @proyek_pendidikans = ProyekPendidikan.all
  end

  # GET /proyek_pendidikans/1
  # GET /proyek_pendidikans/1.json
  def show
  end

  # GET /proyek_pendidikans/new
  def new
    @proyek_pendidikan = ProyekPendidikan.new
  end

  # GET /proyek_pendidikans/1/edit
  def edit
  end

  # POST /proyek_pendidikans
  # POST /proyek_pendidikans.json
  def create
    @proyek_pendidikan = ProyekPendidikan.new(proyek_pendidikan_params)

    respond_to do |format|
      if @proyek_pendidikan.save
        format.html { redirect_to @proyek_pendidikan, notice: 'Proyek pendidikan was successfully created.' }
        format.json { render :show, status: :created, location: @proyek_pendidikan }
      else
        format.html { render :new }
        format.json { render json: @proyek_pendidikan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /proyek_pendidikans/1
  # PATCH/PUT /proyek_pendidikans/1.json
  def update
    respond_to do |format|
      if @proyek_pendidikan.update(proyek_pendidikan_params)
        format.html { redirect_to @proyek_pendidikan, notice: 'Proyek pendidikan was successfully updated.' }
        format.json { render :show, status: :ok, location: @proyek_pendidikan }
      else
        format.html { render :edit }
        format.json { render json: @proyek_pendidikan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /proyek_pendidikans/1
  # DELETE /proyek_pendidikans/1.json
  def destroy
    @proyek_pendidikan.destroy
    respond_to do |format|
      format.html { redirect_to proyek_pendidikans_url, notice: 'Proyek pendidikan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_proyek_pendidikan
      @proyek_pendidikan = ProyekPendidikan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def proyek_pendidikan_params
      params.require(:proyek_pendidikan).permit(:kode_pp, :proyek_pendidikan)
    end
end
