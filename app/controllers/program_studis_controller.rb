class ProgramStudisController < ApplicationController
  before_action :set_program_studi, only: [:show, :edit, :update, :destroy]

  # GET /program_studis
  # GET /program_studis.json
  def index
    @program_studis = ProgramStudi.all
  end

  # GET /program_studis/1
  # GET /program_studis/1.json
  def show
  end

  # GET /program_studis/new
  def new
    @program_studi = ProgramStudi.new
  end

  # GET /program_studis/1/edit
  def edit
  end

  # POST /program_studis
  # POST /program_studis.json
  def create
    @program_studi = ProgramStudi.new(program_studi_params)

    respond_to do |format|
      if @program_studi.save
        format.html { redirect_to @program_studi, notice: 'Program studi was successfully created.' }
        format.json { render :show, status: :created, location: @program_studi }
      else
        format.html { render :new }
        format.json { render json: @program_studi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /program_studis/1
  # PATCH/PUT /program_studis/1.json
  def update
    respond_to do |format|
      if @program_studi.update(program_studi_params)
        format.html { redirect_to @program_studi, notice: 'Program studi was successfully updated.' }
        format.json { render :show, status: :ok, location: @program_studi }
      else
        format.html { render :edit }
        format.json { render json: @program_studi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /program_studis/1
  # DELETE /program_studis/1.json
  def destroy
    @program_studi.destroy
    respond_to do |format|
      format.html { redirect_to program_studis_url, notice: 'Program studi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_program_studi
      @program_studi = ProgramStudi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def program_studi_params
      params.require(:program_studi).permit(:kode_ps, :nama_program_studi)
    end
end
