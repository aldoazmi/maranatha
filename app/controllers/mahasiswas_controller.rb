class MahasiswasController < ApplicationController
  before_action :set_mahasiswa, only: [:show, :edit, :update, :destroy]

  # GET /mahasiswas
  # GET /mahasiswas.json
  def index
    @mahasiswas = Mahasiswa.all
  end

  # GET /mahasiswas/1
  # GET /mahasiswas/1.json
  def show
  end

  # GET /mahasiswas/new
  def new
    @mahasiswa = Mahasiswa.new
  end

  # GET /mahasiswas/1/edit
  def edit
  end

  # POST /mahasiswas
  # POST /mahasiswas.json
  def create
    @mahasiswa = Mahasiswa.new(mahasiswa_params)

    respond_to do |format|
      if @mahasiswa.save
        format.html { redirect_to @mahasiswa, notice: 'Mahasiswa was successfully created.' }
        format.json { render :show, status: :created, location: @mahasiswa }
      else
        format.html { render :new }
        format.json { render json: @mahasiswa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mahasiswas/1
  # PATCH/PUT /mahasiswas/1.json
  def update
    respond_to do |format|
      if @mahasiswa.update(mahasiswa_params)
        format.html { redirect_to @mahasiswa, notice: 'Mahasiswa was successfully updated.' }
        format.json { render :show, status: :ok, location: @mahasiswa }
      else
        format.html { render :edit }
        format.json { render json: @mahasiswa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mahasiswas/1
  # DELETE /mahasiswas/1.json
  def destroy
    @mahasiswa.destroy
    respond_to do |format|
      format.html { redirect_to mahasiswas_url, notice: 'Mahasiswa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mahasiswa
      @mahasiswa = Mahasiswa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mahasiswa_params
      params.require(:mahasiswa).permit(:nrp, :nama, :tanggal_lahir, :tempat_lahir, :jenis_kelamin, :agama, :email, :no_hp, :alamat, :provinsi, :kota_kab, :kode_pos, :kecamatan, :desa_kelurahan, :no_ijazah, :tanggal_ijazah, :nem, :warga_negara, :gol_darah, :rhesus, :jalur_masuk, :status_mahasiswa, :nilai_toefl, :nama_lengkap_ayah, :tanggal_lahir_ayah, :tempat_lahir_ayah, :pekerjaan, :alamat_pekerjaan, :pendidikan_terakhir, :no_hp, :email_ayah, :nama_lengkap_ibu, :tanggal_lahir_ibu, :tempat_lahir_ibu, :pekerjaan_ibu, :alamat_pekerjaan_ibu, :pendidikan_terakhir_ibu, :no_hp_ibu, :email_ibu, :alamat_ortu, :provinsi_ortu, :kota_kab_ortu, :kode_pos_ortu, :kecamatan_ortu, :desa_kelurahan_ortu)
    end
end
