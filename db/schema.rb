# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_26_030740) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dosens", force: :cascade do |t|
    t.string "nik"
    t.string "nama_dosen"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kelas", force: :cascade do |t|
    t.string "kode_kelas"
    t.string "nama_kelas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mahasiswas", force: :cascade do |t|
    t.string "nrp"
    t.string "nama"
    t.date "tanggal_lahir"
    t.string "tempat_lahir"
    t.string "jenis_kelamin"
    t.string "agama"
    t.string "email"
    t.string "no_hp"
    t.string "alamat"
    t.string "provinsi"
    t.string "kota_kab"
    t.string "kode_pos"
    t.string "kecamatan"
    t.string "desa_kelurahan"
    t.string "no_ijazah"
    t.date "tanggal_ijazah"
    t.integer "nem"
    t.string "warga_negara"
    t.string "gol_darah"
    t.string "rhesus"
    t.string "jalur_masuk"
    t.string "status_mahasiswa"
    t.integer "nilai_toefl"
    t.string "nama_lengkap_ayah"
    t.date "tanggal_lahir_ayah"
    t.string "tempat_lahir_ayah"
    t.string "pekerjaan"
    t.string "alamat_pekerjaan"
    t.string "pendidikan_terakhir"
    t.string "email_ayah"
    t.string "nama_lengkap_ibu"
    t.date "tanggal_lahir_ibu"
    t.string "tempat_lahir_ibu"
    t.string "pekerjaan_ibu"
    t.string "alamat_pekerjaan_ibu"
    t.string "pendidikan_terakhir_ibu"
    t.string "no_hp_ibu"
    t.string "email_ibu"
    t.string "alamat_ortu"
    t.string "provinsi_ortu"
    t.string "kota_kab_ortu"
    t.string "kode_pos_ortu"
    t.string "kecamatan_ortu"
    t.string "desa_kelurahan_ortu"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mata_kuliahs", force: :cascade do |t|
    t.string "kode_mata_kuliah"
    t.string "nama_mata_kuliah"
    t.integer "sks"
    t.string "prasyarat"
    t.integer "semester"
    t.string "sifat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "program_studis", force: :cascade do |t|
    t.string "kode_ps"
    t.string "nama_program_studi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proyek_pendidikans", force: :cascade do |t|
    t.string "kode_pp"
    t.string "proyek_pendidikan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
