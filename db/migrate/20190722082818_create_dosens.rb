class CreateDosens < ActiveRecord::Migration[5.2]
  def change
    create_table :dosens do |t|
      t.string :nik
      t.string :nama_dosen
      t.string :email

      t.timestamps
    end
  end
end
