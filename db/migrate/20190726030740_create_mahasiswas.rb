class CreateMahasiswas < ActiveRecord::Migration[5.2]
  def change
    create_table :mahasiswas do |t|
      t.string :nrp
      t.string :nama
      t.date :tanggal_lahir
      t.string :tempat_lahir
      t.string :jenis_kelamin
      t.string :agama
      t.string :email
      t.string :no_hp
      t.string :alamat
      t.string :provinsi
      t.string :kota_kab
      t.string :kode_pos
      t.string :kecamatan
      t.string :desa_kelurahan
      t.string :no_ijazah
      t.date :tanggal_ijazah
      t.integer :nem
      t.string :warga_negara
      t.string :gol_darah
      t.string :rhesus
      t.string :jalur_masuk
      t.string :status_mahasiswa
      t.integer :nilai_toefl
      t.string :nama_lengkap_ayah
      t.date :tanggal_lahir_ayah
      t.string :tempat_lahir_ayah
      t.string :pekerjaan
      t.string :alamat_pekerjaan
      t.string :pendidikan_terakhir
      t.string :no_hp
      t.string :email_ayah
      t.string :nama_lengkap_ibu
      t.date :tanggal_lahir_ibu
      t.string :tempat_lahir_ibu
      t.string :pekerjaan_ibu
      t.string :alamat_pekerjaan_ibu
      t.string :pendidikan_terakhir_ibu
      t.string :no_hp_ibu
      t.string :email_ibu
      t.string :alamat_ortu
      t.string :provinsi_ortu
      t.string :kota_kab_ortu
      t.string :kode_pos_ortu
      t.string :kecamatan_ortu
      t.string :desa_kelurahan_ortu

      t.timestamps
    end
  end
end
