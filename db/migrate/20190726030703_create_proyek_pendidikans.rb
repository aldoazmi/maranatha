class CreateProyekPendidikans < ActiveRecord::Migration[5.2]
  def change
    create_table :proyek_pendidikans do |t|
      t.string :kode_pp
      t.string :proyek_pendidikan

      t.timestamps
    end
  end
end
