class CreateMataKuliahs < ActiveRecord::Migration[5.2]
  def change
    create_table :mata_kuliahs do |t|
      t.string :kode_mata_kuliah
      t.string :nama_mata_kuliah
      t.integer :sks
      t.string :prasyarat
      t.integer :semester
      t.string :sifat

      t.timestamps
    end
  end
end
