class CreateProgramStudis < ActiveRecord::Migration[5.2]
  def change
    create_table :program_studis do |t|
      t.string :kode_ps
      t.string :nama_program_studi

      t.timestamps
    end
  end
end
