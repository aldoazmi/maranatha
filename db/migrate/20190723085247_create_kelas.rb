class CreateKelas < ActiveRecord::Migration[5.2]
  def change
    create_table :kelas do |t|
      t.string :kode_kelas
      t.string :nama_kelas

      t.timestamps
    end
  end
end
